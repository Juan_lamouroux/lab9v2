Use lab4;

CREATE TABLE juegos (
    id INT NOT NULL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    plataforma VARCHAR(50) NOT NULL,
    precio DECIMAL(10,2) NOT NULL
);


INSERT INTO juegos (id,nombre, plataforma, precio) VALUES (1,'The Legend of Zelda: Breath of the Wild', 'Nintendo Switch', 59.99);