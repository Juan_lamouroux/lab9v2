﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Lab4.Model;
using Lab4.Model.DTO;
using Microsoft.EntityFrameworkCore;
using Lab4.Model.persistence;
using Lab4.Model.Services;
using Lab4.Model.Utils;

namespace Lab4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public static UserEntity user = new UserEntity();
        private DatabaseServices context;
        private readonly IConfiguration _configuration;
        private readonly DatabaseServices _context;
        private readonly roleManagement roleManagement= new roleManagement();

        public AuthController(IConfiguration configuration,DatabaseServices context)
        {
            _configuration = configuration;
            _context = context;
        }
        
        [HttpPost("register")]
        public async Task<ActionResult<UserDTO>> RegisterAsync(UserDTO request)
        {
            bool userExists = await _context.users.AnyAsync(u => u.Username == request.Username);
            if (userExists)
            {
                return BadRequest("Username already exists. Please choose another username.");
            }
            UserEntity user = new UserEntity();
            CheckPasswordUser checkPass = new CheckPasswordUser();
            string passwordHash= BCrypt.Net.BCrypt.HashPassword(request.Password);

            user.Username = request.Username;
            user.Password = passwordHash;
            user.Role = roleManagement.isAdmin(request);

            if (!checkPass.passwordIsValid(request.Password)) {
                return BadRequest("The password must be longer than 8 characters and must contain at least one number.");
            } 
            else {
                _context.users.Add(user);
                await _context.SaveChangesAsync();

                return Ok(request);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDTO>> LoginAsync(UserDTO request)
        {
            var user = await _context.users.FirstOrDefaultAsync(u => u.Username == request.Username);
            if (user == null)
            {
                return NotFound();
            }

            if (!BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
            {
                return BadRequest("Wrong password.");
            }

            string token = CreateToken(user);

            return Ok(token);
        }

        private string CreateToken(UserEntity user)
        {   

            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value!));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(7),
                    signingCredentials: creds
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
    }
}
