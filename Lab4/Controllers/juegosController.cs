﻿using Lab4.Model;
using Lab4.Model.DTO;
using Lab4.Model.persistence;
using Lab4.Model.Services;
using Lab4.Model.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Lab4.Controllers
{
    [Route("api/juegos")]
    [ApiController]
    public class juegosController : ControllerBase
    {
        private readonly DatabaseServices _context;
        private CreateMethod create;
        private GetMethod get;
        private UpdateMethod update;
        private DeleteMethod delete;

        private readonly validator validator;

        public juegosController(DatabaseServices context)
        {
            _context = context;
            get = new GetMethod(_context);
            validator = new validator();
        }

        [HttpGet]
        public async Task<ActionResult> GetJuegos()
        {   
            Dictionary<String, Object> Diccionaryjuegos = new Dictionary<String, Object>();
            List<juegoEntity> juegos = await get.Execute();
            Diccionaryjuegos.Add("cantidad:", juegos.Count);
            Diccionaryjuegos.Add("juegos:", juegos);
            return Ok(Diccionaryjuegos);
        }

        [HttpGet("/{name}")]
        [Authorize(Roles = "user, Admin")]
        public async Task<ActionResult> GetJuego(string name)
        {
            var juego = await get.Execute(name);
            if (juego == null)
            {
                return NotFound();
            }
            return Ok(juego);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create(juegoDTO juego)
        {
            create = new CreateMethod(_context);
            // Validación de duplicados por nombre
            if (validator.NullableFields(juego).Count != 0) {
                return BadRequest();
            }
            if (await validator.gameGameExistAsync(_context, juego))
                return Conflict("Ya existe un juego con este mismo nombre");
            
            // Validación de precio no negativo
            if (validator.ValidatePrice(juego))
                return BadRequest("El precio del juego NO puede ser menor a 0");

            // Validación de plataforma
            if (validator.ValidatePlatform(juego))
                return BadRequest("La plataforma debe estar entre Xbox, PlayStation, swith y PC");

            await create.Execute(juego);

            return StatusCode(201);
        }

        [HttpPut("/{name}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Update(string name, juegoDTO juego)
        {
            juegoEntity? juegoExistente = await _context.Juego.FirstOrDefaultAsync(x => x.nombre == name);
            update = new UpdateMethod(_context);
            try
            {
                if (validator.ValidatePrice(juego))
                    return BadRequest("El precio del juego NO puede ser menor a 0");

                if (validator.ValidatePlatform(juego))
                    return BadRequest("La plataforma debe estar entre Xbox, PlayStation, swith y PC");
                juegoExistente.nombre = juego.nombre;
                juegoExistente.precio = juego.precio;
                juegoExistente.plataforma = juego.plataforma;
                
                await update.Execute(juegoExistente);
                return StatusCode(200);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JuegoExists(name))
                    return NotFound();
            }

            return NoContent();
        }


        [HttpDelete("/{name}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string name)
        {
            delete = new DeleteMethod(_context);
            var juego = await _context.Juego.FirstOrDefaultAsync(x => x.nombre == name);

            if (juego == null)
            {
                return NotFound();
            }

            await delete.Execute(name);
        
            return NoContent();
        }
        
        private bool JuegoExists(string name)
        {
            return _context.Juego.Any(e => e.nombre == name);
        }
    }
}
