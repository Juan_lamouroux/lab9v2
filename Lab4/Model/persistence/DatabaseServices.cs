﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Lab4.Model.persistence
{
    [SuppressMessage(
    "TableNamesToLowerCase",
    "SA1300",
    Justification = "It's in lower case since the tables in the db are also in lowerCase."
    )]
    public class DatabaseServices : DbContext
    {

        public DatabaseServices() {
        }

        public DatabaseServices(DbContextOptions<DatabaseServices> options) : base(options)
        {

        }
        public DbSet<juegoEntity> Juego { get; set; }
        public DbSet<UserEntity> users { get; set; }
    }
}
