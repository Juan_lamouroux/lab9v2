﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;


namespace Lab4.Model.configuracion
{
    public class juegoConfiguration
    {
        public juegoConfiguration(EntityTypeBuilder<juegoEntity> entityBuilder)
        {
            
            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.nombre).IsRequired();
            entityBuilder.Property(x => x.plataforma).IsRequired();
            entityBuilder.Property(x => x.precio).IsRequired();
        }
    }
}
