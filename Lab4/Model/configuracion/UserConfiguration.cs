﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;


namespace Lab4.Model.configuracion
{
    public class UserConfiguration
    {
        public UserConfiguration(EntityTypeBuilder<UserEntity> entityBuilder)
        {
            entityBuilder.HasKey(x => x.Id); 
            entityBuilder.Property(x => x.Username)
                .IsRequired() 
                .HasMaxLength(50); 
            entityBuilder.Property(x => x.Password)
                .IsRequired() 
                .HasMaxLength(255);
            entityBuilder.Property(x => x.Role)
                .IsRequired()
                .HasMaxLength(10);

            entityBuilder.HasIndex(x => x.Username).IsUnique();
        }
    }
}
