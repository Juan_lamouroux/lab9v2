﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lab4.Model.DTO
{
    public class UserDTO
    {
        public required string Username { get; set; }
        public required string Password { get; set; }
    }
}
