﻿namespace Lab4.Model.DTO;

public class juegoDTO
{
    public required string nombre { get; set; }
    
    public required string plataforma { get; set; }
    
    public required decimal precio { get; set; }
}