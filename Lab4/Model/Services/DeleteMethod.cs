﻿using Lab4.Model.persistence;
using Microsoft.EntityFrameworkCore;

namespace Lab4.Model.Services
{
    public class DeleteMethod(DatabaseServices context)
    {
        private DatabaseServices _context = context;

        public async Task<bool> Execute(string name)
        {

            var entity = await _context.Juego.FirstOrDefaultAsync(x => x.nombre == name);

            if (entity == null)
                return false;

            _context.Juego.Remove(entity);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
