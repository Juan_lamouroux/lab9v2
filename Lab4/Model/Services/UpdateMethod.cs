﻿using Lab4.Model.persistence;
using Microsoft.EntityFrameworkCore;

namespace Lab4.Model.Services
{
    public class UpdateMethod(DatabaseServices context)
    {
        private DatabaseServices _context = context;

        public async Task<juegoEntity> Execute(juegoEntity model)
        {
            _context.Entry(model).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return model;
        }
    }
}
