﻿using Lab4.Model.persistence;
using Microsoft.EntityFrameworkCore;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Lab4.Model.Services
{
    public class GetMethod(DatabaseServices context)
    {
        private readonly DatabaseServices _context = context;

        public async Task<List<juegoEntity>> Execute()
        {
            return await _context.Juego.ToListAsync();
        }
        public async Task<juegoEntity> Execute(int id)
        {
            return await _context.Juego.FindAsync(id);
        }

        public async Task<juegoEntity> Execute(string name)
        {
            return await _context.Juego.FirstOrDefaultAsync(x => x.nombre == name);
        }

    }
}
