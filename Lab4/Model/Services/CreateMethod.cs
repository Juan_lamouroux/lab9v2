﻿using Lab4.Model.DTO;
using Lab4.Model.persistence;
using AutoMapper;

namespace Lab4.Model.Services
{
    public class CreateMethod(DatabaseServices context)
    {
        private DatabaseServices _context = context;
        private readonly Mapper Mapper;

        public async Task<juegoDTO> Execute(juegoDTO model)
        {
            juegoEntity juego = new juegoEntity();
            juego.nombre = model.nombre;
            juego.precio = model.precio;
            juego.plataforma = model.plataforma;
            _context.Juego.Add(juego);
            await _context.SaveChangesAsync();

            return model;
        }
    }
}
