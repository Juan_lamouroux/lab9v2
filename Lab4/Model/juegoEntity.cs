﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lab4.Model
{
    [Table("juegos")]
    public class juegoEntity
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Column("nombre")]
        [Required]
        public string nombre { get; set; }

        [Column("plataforma")]
        [Required]
        public string plataforma { get; set; }

        [Column("precio")]
        [Required]
        [DataType(DataType.Currency)]
        public decimal precio { get; set; }
    }


}
