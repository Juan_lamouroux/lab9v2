﻿using Lab4.Model.persistence;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Lab4.Model.DTO;

namespace Lab4.Model.Utils
{
    public class validator
    {
        public List<string> NullableFields<T>(T Entity)
        {
            List<string> nullFields = new List<string>();
            Type tipo = typeof(T);
            PropertyInfo[] properties = tipo.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(Entity);
                if ((value == null || string.IsNullOrEmpty(value.ToString())))
                {
                    nullFields.Add(property.Name);
                }
            }

            return nullFields;
        }

        public async Task<bool> gameGameExistAsync(DatabaseServices context,juegoDTO entity ) {
            bool result = false;

            if (await context.Juego.AnyAsync(x => x.nombre == entity.nombre)) {
                result = true;            
            }

            return result;
        }

        public bool ValidatePrice(juegoDTO juego) {
            bool result = false;
            if (juego.precio < 0)
            {
                result = true;
            }

            return result;
        }

        public bool ValidatePlatform(juegoDTO juego)
        {
            bool result = false;
            if (juego.plataforma != "Xbox" && juego.plataforma != "PlayStation" && juego.plataforma != "Switch" && juego.plataforma != "PC")
            {
                result = true;
            }

            return result;
        }

    }
}
