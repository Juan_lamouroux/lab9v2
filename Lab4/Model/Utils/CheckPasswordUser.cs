﻿using System.Text.RegularExpressions;

namespace Lab4.Model.Utils
{
    public class CheckPasswordUser
    {
        public bool passwordIsValid(String password) {
            return password.Length > 8 && Regex.IsMatch(password, @"\d");
        }

    }
}
